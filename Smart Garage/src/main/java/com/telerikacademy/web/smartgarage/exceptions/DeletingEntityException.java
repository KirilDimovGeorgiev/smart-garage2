package com.telerikacademy.web.smartgarage.exceptions;

public class DeletingEntityException extends RuntimeException{
    public DeletingEntityException(String message) {
        super(message);
    }
}
