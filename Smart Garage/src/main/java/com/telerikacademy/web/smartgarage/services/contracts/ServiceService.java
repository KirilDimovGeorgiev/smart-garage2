package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;

import java.util.List;

public interface ServiceService {

    List<Service> getAll(ServiceSearchParameters ssp);

    Service getById(Long id);

    List<Service> getServicesByVisit(Visit visit);

    Service getByName(String name);

    Service create(Service service);

    Service update(Service service);

    void delete(Long id);
}
