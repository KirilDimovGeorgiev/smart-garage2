package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceRepositoryImpl extends AbstractGenericCrudRepository<Service> implements ServiceRepository {

    private final org.hibernate.SessionFactory sessionFactory;

    @Autowired
    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Service> getAll(ServiceSearchParameters ssp) {
        try (Session session = sessionFactory.openSession()) {
            String baseQuery = "from Service where name like concat('%', :name, '%') ";

            Query<Service> query = null;
            if (ssp.getPrice().isPresent()) {
                query = session.createQuery(baseQuery + " and price = :price", Service.class);
                query.setParameter("price", ssp.getPrice().get());
            } else {
                query = session.createQuery(baseQuery, Service.class);
            }
            query.setParameter("name", ssp.getName().orElse(""));

            return query.list();
        }
    }

    @Override
    public List<Service> getServicesByVisit(Visit visit) {
        try (Session session = sessionFactory.openSession()){
            var query = session.createNativeQuery("SELECT s.service_id, s.name, s.price " +
                    " FROM services as s" +
                    " join visits_services as vs on s.service_id = vs.service_id " +
                    " where vs.visit_id = :visitId", Service.class);
            query.setParameter("visitId", visit.getId());

            return query.list();
        }
    }

    @Override
    public Service getById(Long id) {
        return super.getByField("id", id, Service.class);
    }

    @Override
    public Service getByName(String name) {
        return super.getByField("name", name, Service.class);
    }

    @Override
    public Service create(Service service) {
        return super.create(service);
    }

    @Override
    public Service update(Service service) {
        return super.update(service);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, Service.class);
    }
}
