package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl extends AbstractGenericGetRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getById(Long id) {
        return super.getByField("id",id,User.class);
    }

    @Override
    public User getByEmail(String email) {
        return super.getByField("userCredentials.email",email,User.class);
    }
}
