package com.telerikacademy.web.smartgarage.repositories.contracts;


import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;

import java.util.List;

public interface CustomerRepository {
    List<Customer> getAll(CustomerSearchParameters customerSearchParameters);

    Customer getById(Long id);

    Customer getByEmail(String email);

    Customer getByPhone(String phone);

    Customer create(Customer customer);

    Customer update(Customer customer);

    void delete(Long id);

}
