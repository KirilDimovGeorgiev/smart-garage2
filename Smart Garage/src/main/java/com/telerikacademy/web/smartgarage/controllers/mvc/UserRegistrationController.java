package com.telerikacademy.web.smartgarage.controllers.mvc;



import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.models.CustomerDto;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.modelmapper.CustomerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/mvc/registration")
public class UserRegistrationController {


    private final CustomerService customerService;
    private final CustomerModelMapper customerModelMapper;

    @Autowired
    public UserRegistrationController(CustomerService customerService, CustomerModelMapper customerModelMapper) {
        this.customerService = customerService;
        this.customerModelMapper = customerModelMapper;
    }

    @ModelAttribute("customer")
    public CustomerDto userRegistrationDto() {
        return new CustomerDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("customer") @Valid CustomerDto customerDto,
                                      BindingResult result) {

        try {
            customerService.create(customerModelMapper.fromDto(customerDto));
        } catch (DuplicateEntityException e){
            result.rejectValue("email", "email_duplicate", "There is already an account registered with that email");
            return "registration";
        }

        return "redirect:/registration?success";
    }

}
