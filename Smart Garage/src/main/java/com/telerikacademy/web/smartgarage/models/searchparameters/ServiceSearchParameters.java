package com.telerikacademy.web.smartgarage.models.searchparameters;

import java.util.Optional;

public class ServiceSearchParameters {

    private Optional<String> name;

    private Optional<Double> price;

    public ServiceSearchParameters(Optional<String> name, Optional<Double> price) {
        this.name = name;
        this.price = price;
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }

    public Optional<Double> getPrice() {
        return price;
    }

    public void setPrice(Optional<Double> price) {
        this.price = price;
    }
}
