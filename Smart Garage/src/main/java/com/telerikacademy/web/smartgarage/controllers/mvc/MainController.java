package com.telerikacademy.web.smartgarage.controllers.mvc;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/mvc")
    public String root() {
        return "index";
    }

    @GetMapping("/mvc/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/mvc/user")
    public String userIndex() {
        return "user/index";
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @GetMapping("/mvc/customerResource")
    public String getEmployeeResource() {
        return "customer_resource";
    }
}
