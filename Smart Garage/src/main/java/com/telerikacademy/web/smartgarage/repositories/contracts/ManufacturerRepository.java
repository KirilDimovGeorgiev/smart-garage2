package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;

import java.util.List;

public interface ManufacturerRepository {
    List<Manufacturer> getAll(ManufacturerSearchParameters manufacturerSearchParameters);

    Manufacturer getById(Long id);

    Manufacturer getByName(String name);

    Manufacturer create(Manufacturer manufacturer);

    Manufacturer update(Manufacturer manufacturer);

    void delete(Long id);
}
