package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleRepositoryImpl extends AbstractGenericCrudRepository<Vehicle> implements VehicleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory1;
    }

    @Override
    public List<Vehicle> getAll(VehicleSearchParameters vsp) {
        try (Session session = sessionFactory.openSession()) {

            Query<Vehicle> query = session.createQuery("from Vehicle where " +
                    " customer.user.userCredentials.email " +
                    " like concat('%', :email, '%') and licensePlate like concat('%', :licensePlate, '%') " +
                    " and vin like concat('%', :vin, '%') ", Vehicle.class);
            query.setParameter("email", vsp.getCustomerEmail().orElse(""));
            query.setParameter("licensePlate", vsp.getLicensePlate().orElse(""));
            query.setParameter("vin", vsp.getVin().orElse(""));

            return query.list();
        }
    }

    @Override
    public Vehicle getById(Long id) {
        return super.getByField("id", id, Vehicle.class);
    }

    @Override
    public Vehicle getByLicensePlate(String licensePlate) {
        return super.getByField("licensePlate", licensePlate, Vehicle.class);
    }

    @Override
    public Vehicle getByVin(String vin) {
        return super.getByField("vin", vin, Vehicle.class);
    }

    @Override
    public Vehicle create(Vehicle vehicle) {
        return super.create(vehicle);
    }

    @Override
    public Vehicle update(Vehicle vehicle) {
        return super.update(vehicle);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, Vehicle.class);
    }
}
