package com.telerikacademy.web.smartgarage.models.searchparameters;

import java.util.Optional;

public class ManufacturerSearchParameters {

    private Optional<String> name;

    public ManufacturerSearchParameters(Optional<String> name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }
}
