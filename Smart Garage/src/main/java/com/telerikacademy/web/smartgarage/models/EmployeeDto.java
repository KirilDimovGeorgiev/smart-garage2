package com.telerikacademy.web.smartgarage.models;

import javax.validation.constraints.NotNull;

public class EmployeeDto {

    @NotNull(message = "User should not be null.")
    private UserDto user;

    public EmployeeDto() {

    }

    public EmployeeDto(UserDto user) {
        setUser(user);
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
