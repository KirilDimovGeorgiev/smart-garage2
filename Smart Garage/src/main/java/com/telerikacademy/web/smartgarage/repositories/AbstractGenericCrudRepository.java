package com.telerikacademy.web.smartgarage.repositories;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public abstract class AbstractGenericCrudRepository<T> extends AbstractGenericGetRepository<T> {

    private final SessionFactory sessionFactory;

    public AbstractGenericCrudRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public void delete(Long id, Class<T> clazz) {
        T toDelete = getByField("id", id, clazz);
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(toDelete);
            session.getTransaction().commit();
        }
    }

    public T create(T objectToSave) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(objectToSave);
            session.getTransaction().commit();
            return objectToSave;
        }
    }

    public T update(T objectToSave) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(objectToSave);
            tx.commit();
            return objectToSave;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

}
