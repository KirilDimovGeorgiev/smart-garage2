package com.telerikacademy.web.smartgarage.models.searchparameters;

import java.time.LocalDate;
import java.util.Optional;

public class VisitSearchParameters {

    private Optional<LocalDate> date;

    private Optional<LocalDate> untilDate;

    private Optional<String> licensePlate;

    private Optional<String> customerEmail;

    public VisitSearchParameters(Optional<LocalDate> date,
                                 Optional<LocalDate> untilDate,
                                 Optional<String> licensePlate,
                                 Optional<String> customerEmail) {
        this.date = date;
        this.untilDate = untilDate;
        this.licensePlate = licensePlate;
        this.customerEmail = customerEmail;
    }

    public Optional<LocalDate> getDate() {
        return date;
    }

    public void setDate(Optional<LocalDate> date) {
        this.date = date;
    }

    public Optional<LocalDate> getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(Optional<LocalDate> untilDate) {
        this.untilDate = untilDate;
    }

    public Optional<String> getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(Optional<String> licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Optional<String> getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(Optional<String> customerEmail) {
        this.customerEmail = customerEmail;
    }
}
