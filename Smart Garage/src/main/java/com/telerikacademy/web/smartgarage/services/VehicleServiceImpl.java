package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.AbstractGenericCrudRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static java.lang.String.format;

@Service
public class VehicleServiceImpl extends AbstractGenericCrudRepository<Vehicle> implements VehicleService {

    private final VehicleRepository repository;
    private final ModelRepository modelRepository;
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public VehicleServiceImpl(SessionFactory sessionFactory,
                              VehicleRepository repository,
                              ModelRepository modelRepository,
                              ManufacturerRepository manufacturerRepository) {
        super(sessionFactory);
        this.repository = repository;
        this.modelRepository = modelRepository;
        this.manufacturerRepository = manufacturerRepository;
    }

    public List<Vehicle> getAll(VehicleSearchParameters vsp) {
        return repository.getAll(vsp);
    }

    @Override
    public Vehicle getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Vehicle getByLicensePlate(String licensePlate) {
        return repository.getByLicensePlate(licensePlate);
    }

    @Override
    public Vehicle getByVin(String vin) {
        return repository.getByVin(vin);
    }

    @Override
    public Vehicle create(Vehicle vehicle) {

        if (isModelOrManufacturerExists(vehicle)) {
            throw new EntityNotFoundException("model/manufacturer does not exists.");
        }

        if (validateLicensePlate(vehicle)) {
            throw new DuplicateEntityException(format("Vehicle with license plate '%s' already exists.",
                    vehicle.getLicensePlate()));
        }

        if (validateVin(vehicle)) {
            throw new DuplicateEntityException(format("Vehicle with vin '%s' already exists.",
                    vehicle.getVin()));
        }

        return repository.create(vehicle);
    }


    @Override
    public Vehicle update(Vehicle vehicle) {

        if (isModelOrManufacturerExists(vehicle)) {
            throw new EntityNotFoundException("model/manufacturer does not exist.");
        }

        if (validateLicensePlate(vehicle)) {
            throw new DuplicateEntityException(format("Vehicle with license plate '%s' already exists.",
                    vehicle.getLicensePlate()));
        }

        if (validateVin(vehicle)) {
            throw new DuplicateEntityException(format("Vehicle with vin '%s' already exists.",
                    vehicle.getVin()));
        }

        return repository.update(vehicle);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Page<Vehicle> findPaginated(Pageable pageable, VehicleSearchParameters vsp) {

        List<Vehicle> allVehicles = getAll(vsp);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Vehicle> list;

        if (allVehicles.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allVehicles.size());
            list = allVehicles.subList(startItem, toIndex);
        }

        return new PageImpl<Vehicle>(list, PageRequest.of(currentPage, pageSize), allVehicles.size());
    }

    private boolean validateVin(Vehicle vehicle) {
        boolean duplicateExist = true;
        try {
            Vehicle existingVehicle = repository.getByVin(vehicle.getVin());
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }

        return duplicateExist;
    }

    private boolean validateLicensePlate(Vehicle vehicle) {
        boolean duplicateExist = true;
        try {
            Vehicle existingVehicle = repository.getByLicensePlate(vehicle.getLicensePlate());
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }

        return duplicateExist;
    }

    private boolean isModelOrManufacturerExists(Vehicle vehicle) {
        boolean duplicateExist = true;
        try {
            Model existingModel = modelRepository.getByName(vehicle.getModel().getName());
            Manufacturer existingManufacturer = manufacturerRepository
                    .getByName(vehicle.getModel()
                            .getManufacturer().getName());
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }

        return !duplicateExist;
    }
}
