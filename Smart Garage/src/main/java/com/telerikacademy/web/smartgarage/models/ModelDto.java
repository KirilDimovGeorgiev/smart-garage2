package com.telerikacademy.web.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ModelDto {

    @NotNull(message = "Model's name should not be null.")
    @Size(min = 2, max = 20, message = "Model's name should be between 2 and 20 symbols")
    private String name;

    @Positive(message = "Manufacturer id should be positive")
    private Long manufacturerId;

    public ModelDto(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }
}
