package com.telerikacademy.web.smartgarage.models.searchparameters;

import java.util.Optional;

public class VehicleSearchParameters {

    private Optional<String> customerEmail;

    private Optional<String> licensePlate;

    private Optional<String> vin;

    public VehicleSearchParameters(Optional<String> customerEmail,
                                   Optional<String> licensePlate, Optional<String> vin) {
        this.customerEmail = customerEmail;
        this.licensePlate = licensePlate;
        this.vin = vin;
    }

    public Optional<String> getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(Optional<String> licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Optional<String> getVin() {
        return vin;
    }

    public void setVin(Optional<String> vin) {
        this.vin = vin;
    }

    public Optional<String> getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(Optional<String> customerEmail) {
        this.customerEmail = customerEmail;
    }
}
