package com.telerikacademy.web.smartgarage.services.modelmapper;

import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleModelMapper {

    private final VehicleRepository repository;
    private final ModelRepository modelRepository;
    private final CustomerRepository customerRepository;
    private final VehicleTypeRepository vehicleTypeRepository;

    @Autowired
    public VehicleModelMapper(VehicleRepository repository,
                              ModelRepository modelRepository,
                              CustomerRepository customerRepository,
                              VehicleTypeRepository vehicleTypeRepository) {
        this.repository = repository;
        this.modelRepository = modelRepository;
        this.customerRepository = customerRepository;
        this.vehicleTypeRepository = vehicleTypeRepository;
    }

    public Vehicle fromDto(VehicleDto vehicleDto) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(vehicleDto, vehicle);
        return vehicle;
    }

    public Vehicle fromDto(VehicleDto vehicleDto, Long id) {
        Vehicle vehicle = repository.getById(id);
        dtoToObject(vehicleDto, vehicle);
        return vehicle;
    }

    private void dtoToObject(VehicleDto vehicleDto, Vehicle vehicle) {
        Model model = modelRepository.getById(vehicleDto.getModelId());
        Customer customer = customerRepository.getById(vehicleDto.getCustomerId());
        VehicleType vehicleType = vehicleTypeRepository.getById(vehicleDto.getTypeId());

        vehicle.setYear(vehicleDto.getYear());
        vehicle.setVin(vehicleDto.getVin());
        vehicle.setLicensePlate(vehicleDto.getLicensePlate());
        vehicle.setCustomer(customer);
        vehicle.setVehicleType(vehicleType);
        vehicle.setModel(model);
    }
}
