package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Status;

public interface StatusRepository {

    Status getById(Long id);
}
