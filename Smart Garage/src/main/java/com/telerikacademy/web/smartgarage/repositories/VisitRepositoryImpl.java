package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class VisitRepositoryImpl extends AbstractGenericCrudRepository<Visit> implements VisitRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Visit> getAll(VisitSearchParameters vsp) {
        try (Session session = sessionFactory.openSession()) {
            String baseQuery = "from Visit where vehicle.licensePlate like concat('%', :licensePlate, '%') " +
                    " and vehicle.customer.user.userCredentials.email like concat('%', :email, '%') ";

            if (vsp.getDate().isPresent() && vsp.getUntilDate().isEmpty()) {
                baseQuery += " and date = :date";
            } else if (vsp.getDate().isPresent() && vsp.getUntilDate().isPresent()) {
                baseQuery += " and date between :date and :untilDate ";
            }

            Query<Visit> query = session.createQuery(baseQuery, Visit.class);

            if (vsp.getDate().isPresent() && vsp.getUntilDate().isEmpty()) {
                query.setParameter("date", vsp.getDate().get());
            } else if (vsp.getDate().isPresent() && vsp.getUntilDate().isPresent()) {
                query.setParameter("date", vsp.getDate().get());
                query.setParameter("untilDate", vsp.getUntilDate().get());
            }


            query.setParameter("licensePlate", vsp.getLicensePlate().orElse(""));
            query.setParameter("email", vsp.getCustomerEmail().orElse(""));

            return query.list();
        }
    }

    @Override
    public Visit getById(Long id) {
        return super.getByField("id", id, Visit.class);
    }

    @Override
    public Visit getByDate(LocalDate date) {
        return super.getByField("date", date, Visit.class);
    }

    @Override
    public Visit create(Visit visit) {
       return super.create(visit);
    }

    @Override
    public Visit update(Visit visit) {
        return super.update(visit);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, Visit.class);
    }
}
