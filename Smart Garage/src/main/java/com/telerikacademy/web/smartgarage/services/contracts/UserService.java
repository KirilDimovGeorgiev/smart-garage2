package com.telerikacademy.web.smartgarage.services.contracts;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
}
