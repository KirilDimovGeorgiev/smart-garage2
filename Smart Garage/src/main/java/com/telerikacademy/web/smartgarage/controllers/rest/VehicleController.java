package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.VehicleDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.web.smartgarage.services.modelmapper.VehicleModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final VehicleService service;
    private final VehicleModelMapper modelMapper;

    @Autowired
    public VehicleController(VehicleService service, VehicleModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public Vehicle getById(@Valid @PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<Vehicle> getAll(@RequestParam(required = false) Optional<String> email,
                                @RequestParam(required = false) Optional<String> licensePlate,
                                @RequestParam(required = false) Optional<String> vin) {
        return service.getAll(new VehicleSearchParameters(email, licensePlate, vin));
    }

    @PostMapping
    public Vehicle create(@RequestBody @Valid VehicleDto vehicleDto) {
        try {
            Vehicle vehicle = modelMapper.fromDto(vehicleDto);
            return service.create(vehicle);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Vehicle update(@Valid @PathVariable Long id, @Valid @RequestBody VehicleDto vehicleDto) {
        try {
            Vehicle vehicle = modelMapper.fromDto(vehicleDto, id);
            return service.update(vehicle);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
