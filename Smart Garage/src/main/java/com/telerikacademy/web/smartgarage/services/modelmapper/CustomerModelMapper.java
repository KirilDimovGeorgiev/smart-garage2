package com.telerikacademy.web.smartgarage.services.modelmapper;

import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.CustomerDto;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class CustomerModelMapper {
    private final CustomerRepository customerRepository;
    private final UserModelMapper userModelMapper;
    private final RoleRepository roleRepository;

    @Autowired
    public CustomerModelMapper(CustomerRepository customerRepository,
                               UserModelMapper userModelMapper,
                               RoleRepository roleRepository) {
        this.customerRepository = customerRepository;
        this.userModelMapper = userModelMapper;
        this.roleRepository = roleRepository;
    }

    public Customer fromDto(CustomerDto customerDto) {
        Customer customer = new Customer();
        dtoToObject(customerDto, customer);
        return customer;
    }

    public Customer fromDto(CustomerDto customerDto, Long id) {
        Customer customer = customerRepository.getById(id);
        dtoToObjectUpdate(customerDto, customer);
        return customer;
    }

    private void dtoToObject(CustomerDto customerDto, Customer customer) {
        User user = userModelMapper.fromDto(customerDto.getUser());
        customer.setUser(user);
        customer.getUser().setRoles(Set.of(roleRepository.getByName("ROLE_CUSTOMER")));
    }

    private void dtoToObjectUpdate(CustomerDto customerDto, Customer customer) {
        User user = userModelMapper.fromDto(customerDto.getUser(),customer.getUser().getId());
        customer.setUser(user);
    }

    public CustomerDto toDto(Customer customer){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setUser(userModelMapper.toDto(customer.getUser()));
        return customerDto;
    }

}
