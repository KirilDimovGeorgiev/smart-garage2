package com.telerikacademy.web.smartgarage.models.searchparameters;

import java.util.Optional;

public class CustomerSearchParameters {

    private Optional<String> email;

    private Optional<String> firstName;

    private Optional<String> lastName;

    private Optional<String> phone;

    public CustomerSearchParameters(Optional<String> email,
                                    Optional<String> firstName,
                                    Optional<String> lastName,
                                    Optional<String> phone) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public void setLastName(Optional<String> lastName) {
        this.lastName = lastName;
    }

    public Optional<String> getPhone() {
        return phone;
    }

    public void setPhone(Optional<String> phone) {
        this.phone = phone;
    }
}
