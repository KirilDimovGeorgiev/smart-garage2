package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository repository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Service> getAll(ServiceSearchParameters ssp) {
        return repository.getAll(ssp);
    }

    @Override
    public Service getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public List<Service> getServicesByVisit(Visit visit) {
        return repository.getServicesByVisit(visit);
    }

    @Override
    public Service getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public Service create(Service service) {
        boolean duplicateExists = true;

        try {
            Service existingService = getByName(service.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Service", "name", service.getName());
        }

        return repository.create(service);
    }

    @Override
    public Service update(Service service) {
        boolean duplicateExists = true;

        try {
            Service existingService = repository.getByName(service.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Service", "name", service.getName());
        }

        return repository.update(service);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }
}
