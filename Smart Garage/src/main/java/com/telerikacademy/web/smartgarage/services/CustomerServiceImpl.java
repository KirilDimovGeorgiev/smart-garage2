package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.config.SecurityConfig;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;

import com.telerikacademy.web.smartgarage.models.Role;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository repository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Customer> getAll(CustomerSearchParameters customerSearchParameters) {
        return repository.getAll(customerSearchParameters);
    }

    @Override
    public Customer getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Customer getByPhone(String phone) {
        return repository.getByPhone(phone);
    }

    @Override
    public Customer getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public Customer create(Customer customer) {
        validateDuplicate(customer);
        encodePassword(customer);
        return repository.create(customer);
    }

    @Override
    public Customer update(Customer customer) {
        validateDuplicateUpdate(customer);
        encodePassword(customer);
        return repository.update(customer);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Page<Customer> findPaginated(Pageable pageable,CustomerSearchParameters customerSearchParameters) {

        List<Customer> allCustomers = getAll(customerSearchParameters);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Customer> list;

        if (allCustomers.size()< startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allCustomers.size());
            list = allCustomers.subList(startItem, toIndex);
        }

        return new PageImpl<Customer>(list, PageRequest.of(currentPage, pageSize), allCustomers.size());
    }

    private void validateDuplicate(Customer customer){
        boolean duplicateEmailExists = true;
        boolean duplicatePhoneExists = true;

        try {
            repository.getByEmail(customer.getUser().getUserCredentials().getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Customer", "email", customer.getUser().getUserCredentials().getEmail());
        }

        try {
            repository.getByPhone(customer.getUser().getPhone());
        } catch (EntityNotFoundException e) {
            duplicatePhoneExists = false;
        }

        if (duplicatePhoneExists) {
            throw new DuplicateEntityException("Customer", "phone", customer.getUser().getPhone());
        }
    }

    private void validateDuplicateUpdate(Customer customer){
        boolean duplicateEmailExists = true;
        boolean duplicatePhoneExists = true;

        Customer customer1;
        try {
            customer1 = repository.getByEmail(customer.getUser().getUserCredentials().getEmail());
            if(customer1.getId().equals(customer.getId())){
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Customer", "email", customer.getUser().getUserCredentials().getEmail());
        }

        try {
            customer1 = repository.getByPhone(customer.getUser().getPhone());
            if(customer1.getId().equals(customer.getId())){
                duplicatePhoneExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicatePhoneExists = false;
        }

        if (duplicatePhoneExists) {
            throw new DuplicateEntityException("Customer", "phone", customer.getUser().getPhone());
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = repository.getByEmail(username);
        if (customer == null){
            throw new UsernameNotFoundException("Invalid email or password.");
        }
        return new org.springframework.security.core.userdetails.User(customer.getUser().getUserCredentials().getEmail(),
                customer.getUser().getUserCredentials().getPassword(),
                mapRolesToAuthorities(customer.getUser().getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    private void encodePassword(Customer customer){
        customer.getUser().getUserCredentials().setPassword(
                SecurityConfig.passwordEncoder().encode(customer.getUser().getUserCredentials().getPassword())
        );
    }
}
