package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.VisitDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import com.telerikacademy.web.smartgarage.services.modelmapper.VisitModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private final VisitService service;
    private final VisitModelMapper modelMapper;

    @Autowired
    public VisitController(VisitService service, VisitModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Visit> getAll(@RequestParam(required = false)
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> date,
                              @RequestParam(required = false) Optional<String> licensePlate,
                              @RequestParam(required = false) Optional<String> customerEmail,
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> untilDate) {
        return service.getAll(new VisitSearchParameters(date, untilDate, licensePlate, customerEmail));
    }
    @GetMapping("/{id}")
    public Visit getById(@Valid @PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Visit create(@Valid @RequestBody VisitDto visitDto) {
        Visit visit = modelMapper.fromDto(visitDto);
        return service.create(visit);
    }

    @PutMapping("/{id}")
    public Visit update(@Valid @PathVariable Long id, @Valid @RequestBody VisitDto visitDto) {
        Visit visit = modelMapper.fromDto(visitDto, id);
        return service.update(visit);
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
