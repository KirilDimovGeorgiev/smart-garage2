package com.telerikacademy.web.smartgarage.services.modelmapper;

import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.repositories.contracts.StatusRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VisitModelMapper {

    private final VisitRepository repository;
    private final StatusRepository statusRepository;
   private final VehicleRepository vehicleRepository;

    @Autowired
    public VisitModelMapper(VisitRepository repository,
                            StatusRepository statusRepository,
                            VehicleRepository vehicleRepository) {
        this.repository = repository;
        this.statusRepository = statusRepository;
        this.vehicleRepository = vehicleRepository;
    }

    public Visit fromDto(VisitDto visitDto) {
        Visit visit = new Visit();
        dtoToObject(visitDto, visit);
        return visit;
    }

    public Visit fromDto(VisitDto visitDto, Long id) {
        Visit visit = repository.getById(id);
        dtoToObject(visitDto, visit);
        return visit;
    }

    private void dtoToObject(VisitDto visitDto, Visit visit) {
        Vehicle vehicle = vehicleRepository.getById(visitDto.getVehicleId());
        Status status = statusRepository.getById(visitDto.getStatusId());

        visit.setDate(visitDto.getDate());
        visit.setVehicle(vehicle);
        visit.setStatus(status);
    }

}
