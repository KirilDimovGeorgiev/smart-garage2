package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;

import java.time.LocalDate;
import java.util.List;

public interface VisitService {

    List<Visit> getAll(VisitSearchParameters vsp);

    Visit getById(Long id);

    Visit getByDate(LocalDate date);

    Visit create(Visit visit);

    Visit update(Visit visit);

    void delete(Long id);
}
