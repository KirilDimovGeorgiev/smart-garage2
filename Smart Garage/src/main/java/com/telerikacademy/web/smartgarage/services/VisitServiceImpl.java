package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitRepository repository;

    @Autowired
    public VisitServiceImpl(VisitRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Visit> getAll(VisitSearchParameters vsp) {
        return repository.getAll(vsp);
    }

    @Override
    public Visit getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Visit getByDate(LocalDate date) {
        return repository.getByDate(date);
    }

    @Override
    public Visit create(Visit visit) {
        return repository.create(visit);
    }

    @Override
    public Visit update(Visit visit) {
        return repository.update(visit);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }
}
