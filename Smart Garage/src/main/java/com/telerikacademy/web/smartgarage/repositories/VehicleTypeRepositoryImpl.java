package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.VehicleType;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class VehicleTypeRepositoryImpl extends AbstractGenericGetRepository<VehicleType>
        implements VehicleTypeRepository {

    public VehicleTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public VehicleType getById(Long id) {
        return super.getByField("id", id, VehicleType.class);
    }
}
