package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.EmployeeRepository;
import com.telerikacademy.web.smartgarage.services.contracts.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository repository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Employee> getAll(EmployeeSearchParameters employeeSearchParameters){
        return repository.getAll(employeeSearchParameters);
    }

    @Override
    public Employee getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Employee getByPhone(String phone) {
        return repository.getByPhone(phone);
    }

    @Override
    public Employee getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public Employee create(Employee employee) {
        validateDuplicate(employee);
        return repository.create(employee);
    }

    @Override
    public Employee update(Employee employee) {
        validateDuplicateUpdate(employee);
        return repository.update(employee);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Page<Employee> findPaginated(Pageable pageable, EmployeeSearchParameters employeeSearchParameters) {

        List<Employee> allEmployees = getAll(employeeSearchParameters);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Employee> list;

        if (allEmployees.size()< startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allEmployees.size());
            list = allEmployees.subList(startItem, toIndex);
        }

        return new PageImpl<Employee>(list, PageRequest.of(currentPage, pageSize), allEmployees.size());
    }

    private void validateDuplicate(Employee employee){

        boolean duplicateEmailExists = true;
        boolean duplicatePhoneExists = true;

        try {
            repository.getByEmail(employee.getUser().getUserCredentials().getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Employee", "email", employee.getUser().getUserCredentials().getEmail());
        }

        try {
            repository.getByPhone(employee.getUser().getPhone());
        } catch (EntityNotFoundException e) {
            duplicatePhoneExists = false;
        }

        if (duplicatePhoneExists) {
            throw new DuplicateEntityException("Employee", "phone", employee.getUser().getPhone());
        }
    }

    private void validateDuplicateUpdate(Employee employee){

        boolean duplicateEmailExists = true;
        boolean duplicatePhoneExists = true;

        Employee employee1;
        try {
             employee1 = repository.getByEmail(employee.getUser().getUserCredentials().getEmail());
            if(employee1.getId().equals(employee.getId())){
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Employee", "email", employee.getUser().getUserCredentials().getEmail());
        }

        try {
            employee1 = repository.getByPhone(employee.getUser().getPhone());
            if(employee1.getId().equals(employee.getId())){
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicatePhoneExists = false;
        }

        if (duplicatePhoneExists) {
            throw new DuplicateEntityException("Employee", "phone", employee.getUser().getPhone());
        }
    }
}
