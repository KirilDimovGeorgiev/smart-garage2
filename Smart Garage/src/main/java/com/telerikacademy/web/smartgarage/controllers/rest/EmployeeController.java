package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.EmployeeDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.EmployeeService;
import com.telerikacademy.web.smartgarage.services.modelmapper.EmployeeModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/employees")
public class EmployeeController {
    private final EmployeeService service;
    private final EmployeeModelMapper modelMapper;


    @Autowired
    public EmployeeController(EmployeeService service, EmployeeModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Employee> getAll(@RequestParam(required = false) Optional<String> email,
                                 @RequestParam(required = false) Optional<String> firstName,
                                 @RequestParam(required = false) Optional<String> lastName,
                                 @RequestParam(required = false) Optional<String> phone) {
        return service.getAll(new EmployeeSearchParameters(email,firstName,lastName,phone));
    }

    @GetMapping("/{id}")
    public Employee getById(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/email/{email}")
    public Employee getByEmail(@RequestHeader HttpHeaders headers,@PathVariable String email) {
        try {
            return service.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/phone/{phone}")
    public Employee getByPhone(@RequestHeader HttpHeaders headers,@PathVariable String phone) {
        try {
            return service.getByPhone(phone);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    public Employee create(@RequestHeader HttpHeaders headers, @Valid @RequestBody EmployeeDto employeeDto) {
        try {
            Employee employee = modelMapper.fromDto(employeeDto);
            return service.create(employee);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public Employee update(@RequestHeader HttpHeaders headers, @PathVariable Long id, @Valid @RequestBody EmployeeDto employeeDto) {
        try {
            Employee employee = modelMapper.fromDto(employeeDto, id);
            return service.update(employee);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
