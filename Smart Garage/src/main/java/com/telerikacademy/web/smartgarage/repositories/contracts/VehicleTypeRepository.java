package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.VehicleType;

public interface VehicleTypeRepository {

    VehicleType getById(Long id);
}
