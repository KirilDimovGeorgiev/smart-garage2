package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepositoryImpl extends AbstractGenericCrudRepository<Customer> implements CustomerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Customer> getAll(CustomerSearchParameters customerSearchParameters) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer as c " +
                            "where c.user.firstName like concat('%', :firstName,'%') " +
                            "and c.user.lastName like concat('%', :lastName,'%') " +
                            "and c.user.userCredentials.email like concat('%', :email,'%') " +
                            "and c.user.phone like concat('%', :phone,'%')",
                    Customer.class);
            query.setParameter("firstName", customerSearchParameters.getFirstName().orElse(""));
            query.setParameter("lastName", customerSearchParameters.getLastName().orElse(""));
            query.setParameter("email", customerSearchParameters.getEmail().orElse(""));
            query.setParameter("phone", customerSearchParameters.getPhone().orElse(""));

            return query.list();
        }
    }

    @Override
    public Customer getById(Long id) {
        return super.getByField("id",id,Customer.class);
    }

    @Override
    public Customer getByEmail(String email) {
        return super.getByField("user.userCredentials.email",email,Customer.class);
    }

    @Override
    public Customer getByPhone(String phone) {
        return super.getByField("user.phone",phone,Customer.class);
    }

    @Override
    public Customer create(Customer customer){
        return super.create(customer);
    }

    @Override
    public Customer update(Customer customer){
        return super.update(customer);
    }

    @Override
    public void delete(Long id) {
         super.delete(id,Customer.class);
    }
}
