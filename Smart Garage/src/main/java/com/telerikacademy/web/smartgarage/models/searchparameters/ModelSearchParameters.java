package com.telerikacademy.web.smartgarage.models.searchparameters;

import java.util.Optional;

public class ModelSearchParameters {

    private Optional<String> name;

    private Optional<String> manufacturerName;

    public ModelSearchParameters(Optional<String> name, Optional<String> manufacturerName) {
        this.name = name;
        this.manufacturerName = manufacturerName;
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }

    public Optional<String> getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(Optional<String> manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
}
