package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;

import java.util.List;

public interface ModelRepository {

    List<Model> getAll(ModelSearchParameters modelSearchParameters);

    Model getById(Long id);

    Model getByName(String name);

    Model create(Model model);

    Model update(Model model);

    void delete(Long id);
}
