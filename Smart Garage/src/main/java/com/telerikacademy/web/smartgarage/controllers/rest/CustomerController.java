package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.CustomerDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.modelmapper.CustomerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/customers")
public class CustomerController {
    private final CustomerService service;
    private final CustomerModelMapper modelMapper;

    @Autowired
    public CustomerController(CustomerService service, CustomerModelMapper modelMapper ) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Customer> getAll(@RequestParam(required = false) Optional<String> email,
                                 @RequestParam(required = false) Optional<String> firstName,
                                 @RequestParam(required = false) Optional<String> lastName,
                                 @RequestParam(required = false) Optional<String> phone) {
        return service.getAll(new CustomerSearchParameters(email,firstName,lastName,phone));
    }

    @GetMapping("/numberOfCustomers")
    public Integer getNumberOfCustomers() {
        return service.getAll(new CustomerSearchParameters(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty())).size();
    }

    @GetMapping("/{id}")
    public Customer getById(@RequestHeader HttpHeaders headers,@PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/email/{email}")
    public Customer getByEmail(@RequestHeader HttpHeaders headers,@PathVariable String email) {
        try {
            return service.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/phone/{phone}")
    public Customer getByUsername(@RequestHeader HttpHeaders headers,@PathVariable String phone) {
        try {
            return service.getByPhone(phone);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    public Customer create(@Valid @RequestBody CustomerDto customerDto) {
        try {
            Customer customer = modelMapper.fromDto(customerDto);
            return service.create(customer);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public Customer update(@RequestHeader HttpHeaders headers,@PathVariable Long id, @Valid @RequestBody CustomerDto customerDto) {
        try {
            Customer customer = modelMapper.fromDto(customerDto, id);
            return service.update(customer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
