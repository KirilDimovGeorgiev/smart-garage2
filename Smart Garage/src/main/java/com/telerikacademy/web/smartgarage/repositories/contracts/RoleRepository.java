package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Role;

public interface RoleRepository {
    Role getById(Long id);

    Role getByName(String name);
}
