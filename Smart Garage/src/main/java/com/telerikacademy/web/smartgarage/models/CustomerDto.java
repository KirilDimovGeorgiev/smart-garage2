package com.telerikacademy.web.smartgarage.models;

import javax.validation.constraints.NotNull;

public class CustomerDto {

    @NotNull(message = "User should not be null.")
    private UserDto user;

    public CustomerDto() {
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
