package com.telerikacademy.web.smartgarage.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.smartgarage.services.CustomerServiceImpl;
import com.telerikacademy.web.smartgarage.smartgarage.helpers.CustomerHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTests {

    @Mock
    CustomerRepository repository;
    
    @InjectMocks
    CustomerServiceImpl service;


    @Test
    public void create_Should_Throw_Exception_WhenEmailExists() {
        var mockCustomer = CustomerHelper.createMockCustomer();

        when(repository.getByEmail(mockCustomer.getUser().getUserCredentials().getEmail())).thenReturn(mockCustomer);

        assertThrows(DuplicateEntityException.class, () -> service.create(mockCustomer));
    }
}
