package com.telerikacademy.web.smartgarage.smartgarage.helpers;

import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.Role;
import com.telerikacademy.web.smartgarage.models.User;

import java.util.Set;

public class CustomerHelper {

    public static Customer createMockCustomer() {
        var mockCustomer = new Customer();
        mockCustomer.setId(1L);
        mockCustomer.setUser(createMockUser());
        return mockCustomer;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1L);
        mockUser.getUserCredentials().setEmail("mock@email.com");
        mockUser.setFirstName("mockFirstName");
        mockUser.setLastName("mockLastName");
        mockUser.getUserCredentials().setPassword("mockPassword");
        mockUser.setPhone("0987654321");
        mockUser.setRoles(Set.of(createMockRole()));
        return mockUser;
    }

    public static Role createMockRole() {
        var mockRole = new Role();
        mockRole.setName("Customer");
        mockRole.setId(1L);
        return mockRole;
    }


}
