create table manufacturers
(
    manufacturer_id bigint auto_increment
        primary key,
    name            varchar(20) not null,
    constraint manufacturers_name_uindex
        unique (name)
);

create table models
(
    model_id        bigint auto_increment
        primary key,
    manufacturer_id bigint      not null,
    name            varchar(20) not null,
    constraint models_varchar_uindex
        unique (name),
    constraint models_manufacturer_fk
        foreign key (manufacturer_id) references manufacturers (manufacturer_id)
);

create table roles
(
    role_id bigint auto_increment
        primary key,
    name    varchar(20) not null,
    constraint role_name_uindex
        unique (name)
);

create table services
(
    service_id bigint auto_increment
        primary key,
    name       varchar(20) not null,
    price      double      not null,
    constraint services_name_uindex
        unique (name)
);

create table statuses
(
    status_id bigint auto_increment
        primary key,
    name      varchar(20) not null,
    constraint statuses_name_uindex
        unique (name)
);

create table users
(
    user_id   bigint auto_increment
        primary key,
    email     varchar(30) not null,
    password  varchar(30) not null,
    firstName varchar(20) not null,
    lastName  varchar(20) not null,
    phone     varchar(10) not null,
    constraint users_email_uindex
        unique (email),
    constraint users_phone_uindex
        unique (phone)
);

create table customers
(
    customer_id bigint auto_increment
        primary key,
    user_id     bigint not null,
    constraint customers_users_fk
        foreign key (user_id) references users (user_id)
);

create table employees
(
    employee_id bigint auto_increment
        primary key,
    user_id     bigint not null,
    constraint employees_users_fk
        foreign key (user_id) references users (user_id)
);

create table users_roles
(
    user_id bigint not null,
    role_id bigint not null,
    constraint users_roles_roles_fk
        foreign key (role_id) references roles (role_id),
    constraint users_roles_users_fk
        foreign key (user_id) references users (user_id)
);

create table vehicle_types
(
    vehicle_type_id bigint auto_increment
        primary key,
    name            varchar(20) not null,
    constraint vehicle_types_name_uindex
        unique (name)
);

create table vehicles
(
    vehicle_id      bigint auto_increment
        primary key,
    `license plate` varchar(10) not null,
    year            bigint      not null,
    vin             varchar(17) not null,
    model_id        bigint      not null,
    customer_id     bigint      not null,
    vehicle_type_id bigint      not null,
    constraint `vehicles2_license plate_uindex`
        unique (`license plate`),
    constraint vehicles2_vin_uindex
        unique (vin),
    constraint vehicles_customers_fk
        foreign key (customer_id) references customers (customer_id),
    constraint vehicles_models_fk
        foreign key (model_id) references models (model_id),
    constraint vehicles_vehicle_type_fk
        foreign key (vehicle_type_id) references vehicle_types (vehicle_type_id)
);

create table visits
(
    visit_id   bigint auto_increment
        primary key,
    date       timestamp default current_timestamp() not null on update current_timestamp(),
    vehicle_id bigint                                not null,
    status_id  bigint                                not null,
    constraint visits_status_fk
        foreign key (status_id) references statuses (status_id),
    constraint visits_vehicles_fk
        foreign key (vehicle_id) references vehicles (vehicle_id)
);

create table visits_services
(
    visit_id   bigint not null,
    service_id bigint not null,
    constraint visits_services_services_fk
        foreign key (service_id) references services (service_id),
    constraint visits_services_visits_fk
        foreign key (visit_id) references visits (visit_id)
);
